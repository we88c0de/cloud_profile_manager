package cmd

import (
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/julienlevasseur/cloud_profile_manager/pkg/profile"
)

/*RootCmd root command*/
var RootCmd = &cobra.Command{
	Use:   "cloud_profile_manager",
	Short: "A tool to manage your env vars as profiles.",
	Long: `Cloud Profile Manager is simple tool that allow you to manage your
	environment variables for cloud tools (such as kubectl, awscli, openstack
	clients, terraform, consul ...).`,
	Run: func(cmd *cobra.Command, args []string) {
		profile.UseNoProfile()
	},
}

/*Execute is used in main.go*/
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

func initConfig() {
	if os.Getenv("CLOUD_PROFILE_MANAGER_CFG") != "" {
		viper.SetConfigFile(os.Getenv("CLOUD_PROFILE_MANAGER_CFG"))
	} else {
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		viper.SetConfigFile(fmt.Sprintf("%s/.cloud_profile_manager_cfg.yml", home))
		viper.SetDefault("shell", os.Getenv("SHELL"))
		viper.SetDefault("preserveCloudProfile", true)
	}

	viper.AutomaticEnv()
	viper.SetConfigType("yaml")

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
	}
}
