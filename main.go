package main

import (
	"gitlab.com/julienlevasseur/cloud_profile_manager/cmd"
)

func main() {
	cmd.Execute()
}
