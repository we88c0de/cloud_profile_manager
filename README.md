# Cloud Profile Manager

[![Build status](https://gitlab.com/julienlevasseur/cloud_profile_manager/badges/master/build.svg)](https://gitlab.com/julienlevasseur/cloud_profile_manager/commits/master)

![cloud_profile_manager.png](https://gitlab.com/julienlevasseur/cloud_profile_manager/raw/master/images/cloud_profile_manager.png)

Cloud Profile Manager is simple tool that allow you to manage your environment variables for cloud tools (such as kubectl, awscli, openstack clients, terraform, consul ...).

## Usage

### The profile file

A profile file is a simple YAML file that represent the vars you need for this profile :

```yaml
profile_name: aws_dev
shell: /usr/bin/zsh
AWS_ACCESS_KEY_ID: xxxxxxxxXXXXXxxxxxxxx
AWS_SECRET_ACCESS_KEY: xxxxxxxxXXXXXxxxxxxxx
AWS_DEFAULT_REGION: us-east-1
CONSUL_HTTP_TOKEN: xxxxxxxxXXXXXxxxxxxxx
TF_VAR_CONSUL_HTTP_TOKEN: xxxxxxxxXXXXXxxxxxxxx
```

Those profile files have to be located in `` and named like `.FooBar.yml`.

### The config file

The config file is located by default in `~/.cloud_profile_manager_cfg.yml`.
You can override this value by setting the `CLOUD_PROFILE_MANAGER_CFG` env var:

```bash
export CLOUD_PROFILE_MANAGER_CFG="/my/prefered/path"
```

> **Note**
> 
> If no configuration file is found, a default configuration file will be created poiting the `cloudProfileFolder` attribute to `$HOME/.cloud_profile_manager`.

#### Configuration options

##### shell

The cloud_profile file may contain the `shell` attribute. This attribute will never be exported as env variable. Its used to specify in which shell you want to spawn your profile.

You can also set a `shell` in the configuration file. This can be helpful if you want to use a diferent shell than your current one when you use a profile.

> **Note**
>
> The default shell is the current shell.

> **Note**
>
> Cloud Profile Manager hsa been tested only with bash and zsh.
> Any contribution to validate other shells are welcomed.

##### preserveCloudProfile

This option allow you to decide if you want to preserve the `.cloud_profile` file where you have used a profile or remove it once the profile is exported.
With this option you can decide if you prefer to keep the `.cloud_profile` files, so you can re-use a profile later (adding it to your global `.gitignore` is strongly recommended) or simply decide that you want to generate it every time.


###### Example of a configuration file

```yml
cloudProfileFolder: /My/Home/.cloud_profile_manager
shell: bash               # Optional (current shell by default)
preserveCloudProfile: true # Optional (true by default)
```

### The profile definition

If you want to set env vars or cloud profiles, you can create as many profile files as you want into the `cloudProfileFolder`.

> **Note**
>
> **Be carefull !** The profiles files have to be hidden (so prefixed by a `.`)
>
> e.g:
> ```bash
>/My/Home/.cloud_profile_manager/
>└── .example-aws-us-east-1.yml
>```
> 

### The yaml env file

Inspired by [direnv](https://direnv.net/), this feature allow you to create a `.env.yml` file into a folder that will be sourced when you call this tool inside this folder.

When you call `cloud_profile_manager` without arguments, the program will look on the current working directory for a file called `.env.yml` and source it if found.

When you call `cloud_profile_manager use ${profile}`, the program will also look for `.env/yml` and append its content to the specified profile.

This feature is usefull if you want to have immutable set a vars for a cloud provider (the profile) but specific vars for a specific project, repo, branch, etc
For example, you can want to set your AWS keys in the `my_aws_account` profile, but having different `AWS_DEFAULT_REGION` or dedicated Terraform vars in several projects.
To accomplish that, just create a `/etc/cloud_profile/.my_aws_account.yml` profile file and in your repos, a .env.yml per repo with the dedicated set of vars inside.

Example of a `.env.yml` file:

```yaml
AWS_DEFAULT_REGION: us-east-2
TF_VAR_project_name: my_awesome_project
```

> **Note:** The `.env.yml` file override the double env vars that it can find in the profile.

### .envrc support

To go deeper into the `direnv` inspiration/compatibility, cloud_profile_manager now support `.envrc` files.

Example of a `.envrc` file:

```bash
export FOO=bar
```

### .env support

To complete the non `yaml` files support, the 1.3.0 version has seen the addition of `*.env` files support.

Example of `.env` file:

```bash
export FOO=bar
```

### The cloud_profile_manager command

* `cloud_profile_manager` - Search for env files and source them if they exists.
* `cloud_profile_manager` `list` - list the available profiles.
* `cloud_profile_manager` `use` - Search for .cloud_profile file and env files and export the generated profile from them.
* `cloud_profile_manager` `use` `${profile_name}` - Actually use the specified profile.
* `cloud_profile_manager` `aws_mfa` `${MFA Token}` - Need an already exported AWS profile. Authenticate to AWS with MFA Token. (Surcharge the current profile with Secret Key, Access Key Id and Token from MFA auth.)
* `cloud_profile_manager` `help` - Display the help message.

## Tips

> **Note**
> I strongly recommand, for convenience, for you to add `.cloud_profile` fiel to your global `.gitignore`.

## Dependencies installation

This command will install the dependencies, including the test ones :
```bash
go get -t ./...
```

## How I'm using it

Basically I like the idea to run a command that summerize all the env vars I need than sourcing a file in a folder that I will haveto figure out the location almost every time I need it.

So, I define my cloud providers env vars per profile :

* Work AWS
* Work OpenStack
* Personnal AWS
...

In each of those, I have something like :

```yaml
profile_name: aws_dev
AWS_ACCESS_KEY_ID: 
AWS_SECRET_ACCESS_KEY: 
AWS_DEFAULT_REGION: us-east-1
CONSUL_HTTP_TOKEN: 
```
I'm using the `profile_name` var in my PS1 to display on my shell which profile is currently in use :

![usage_demo.png](https://gitlab.com/julienlevasseur/cloud_profile_manager/raw/master/images/usage_demo.png)

And I set project's specific vars via the `.env.yml` file. For example, if I have a project that contain the code to provision a Kubernetes cluster with [KOPS](https://github.com/kubernetes/kops) I will set the `my_project/.env.yml` as :

```yaml
KOPS_STATE_STORE: s3://my-project-kubernetes-aws-state
KUBE_CTX_CLUSTER: my-project-k8s.my.domain.me
KUBE_USER: admin
KUBE_PASSWORD: ***************
```

### AWS MFA

![aws_mfa_demo.png](https://gitlab.com/julienlevasseur/cloud_profile_manager/raw/master/images/aws_mfa_demo.png)

> **Note**
> The AWS MFA feature rely on the `AWS_MFA_USERNAME` env var. Be sure to have it set in your profile prior to use aws_mfa option.

## Concept summary

![concept_summary.png](https://gitlab.com/julienlevasseur/cloud_profile_manager/raw/master/images/concept_summary.png)
