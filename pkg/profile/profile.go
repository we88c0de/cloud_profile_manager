package profile

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"

	yaml "gopkg.in/yaml.v2"

	"github.com/spf13/viper"

	"gitlab.com/julienlevasseur/cloud_profile_manager/helpers"
)

type keyValueMap map[string]string

var envVars keyValueMap
var cloudProfileFile, _ = filepath.Abs(".cloud_profile")
var anyEnvFile = helpers.ListFiles(".", "*.env")
var envFile, _ = filepath.Abs(".env.yml")
var envRcFile, _ = filepath.Abs(".envrc")

func fileExist(file string) bool {
	if _, err := os.Stat(file); err == nil {
		return true
	}

	return false
}

func parseYaml(filename string) keyValueMap {
	var y keyValueMap
	source, err := ioutil.ReadFile((filename))
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(source, &y)
	if err != nil {
		panic(err)
	}

	return y
}

func parseEnvrc(filename string) keyValueMap {
	envrcVars := make(map[string]string)
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		envrc := strings.Split(scanner.Text(), "export ")
		for _, export := range envrc {
			if len(export) == 0 {
				continue
			} else {
				envrcVars[strings.Split(export, "=")[0]] = strings.Replace(strings.Split(export, "=")[1], "\"", "", -1)
			}
		}
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return envrcVars
}

/*SetEnvironment read the cloudProfileFile and set a new environment in
  the given shell (exported one if the config doesn't specify one)*/
func SetEnvironment(yml keyValueMap) {
	d := []byte("")
	err := ioutil.WriteFile(cloudProfileFile, d, 0644)
	if err != nil {
		panic(err)
	}

	shell := viper.GetString("shell")

	for k, v := range yml {

		file, err := os.OpenFile(cloudProfileFile, os.O_APPEND|os.O_WRONLY, 0644)

		if err != nil {
			panic(err)
		}

		defer file.Close()

		str := fmt.Sprintf("export %s=\"%v\"\n", k, v)
		if _, err = file.WriteString(str); err != nil {
			panic(err)
		}

		os.Setenv(k, v)
	}

	binary, err := exec.LookPath(shell)

	if err != nil {
		panic(err)
	}

	if viper.GetBool("preserveCloudProfile") == false {
		helpers.CleanProfileFile()
	}

	env := os.Environ()
	args := []string{shell}
	err = syscall.Exec(binary, args, env)

	if err != nil {
		panic(err)
	}
}

func getProfile(profileFolder string, profileName string) keyValueMap {
	return parseYaml(
		fmt.Sprintf(
			"%s/.%v.yml",
			profileFolder,
			profileName,
		),
	)
}

/*Use return a map of all the key:value set found in the local accpeted
files, including the given profile*/
func Use(profilesFolder string, profileName string) {
	envVars := make(map[string]string)
	// parse .cloud_profile file:
	for k, v := range getProfile(profilesFolder, profileName) {
		envVars[k] = v
	}
	// check for any .env files:
	for _, thisEnvFile := range anyEnvFile {
		for k, v := range parseEnvrc(thisEnvFile) {
			envVars[k] = v
		}
	}
	// check for .env.yml file:
	if fileExist(envFile) {
		for k, v := range parseYaml(envFile) {
			envVars[k] = v
		}
	}
	// check for .envrc file:
	if fileExist(envRcFile) {
		for k, v := range parseEnvrc(envRcFile) {
			envVars[k] = v
		}
	}

	SetEnvironment(envVars)
}

/*UseNoProfile return a map of all the key:value set found in the local accpeted
files*/
func UseNoProfile() {
	envVars := make(map[string]string)
	// check for .cloud_profile file:
	if fileExist(cloudProfileFile) {
		for k, v := range parseEnvrc(cloudProfileFile) {
			envVars[k] = v
		}
	}
	// check for any .env files:
	for _, thisEnvFile := range anyEnvFile {
		for k, v := range parseEnvrc(thisEnvFile) {
			envVars[k] = v
		}
	}
	// check for .env.yml file:
	if fileExist(envFile) {
		for k, v := range parseYaml(envFile) {
			envVars[k] = v
		}
	}
	// check for .envrc file:
	if fileExist(envRcFile) {
		for k, v := range parseEnvrc(envRcFile) {
			envVars[k] = v
		}
	}

	SetEnvironment(envVars)
}
